<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<style type="text/css">
<!--
@import url("<c:url value='/static/style.css' />");
-->
</style>
</head>

<body>
	<div id="loginPage">


		<span style="float: right" id="langButtons"> <a href="?lang=en"><img
				src="<c:url value='/static/en.gif' />" /></a> | <a href="?lang=et"><img
				src="<c:url value='/static/et.gif' />" /></a>
		</span> <br /> <br />

		<c:if test="${not empty error}">
		<div id="messageBlock" class="errorBlock">
        <spring:message code="${error}" />
        </div>
		</c:if>

		<form action="<c:url value="j_spring_security_check" />" method="POST">
			<spring:message code="login.username" />
			: <input type="text" id="usernameBox" name="j_username" value=""><br>
			<spring:message code="login.password" />
			: <input type="password" id="passwordBox" name="j_password" value="" /><br>
			<input type="submit" id="logInButton"
				value="<spring:message code='login.button' />" />
		</form>

	</div>

</body>
</html>
