<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:url value="/" var="base" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SpringDemo</title>
<style type="text/css">
<!--
@import url("<c:url value='/static/style.css' />");
-->
</style>
</head>
<body>
	<%@ include file="menu.jsp"%>
	<form:form method="post" action="search" modelAttribute="unitForm">
		<form:input name="searchString" id="searchStringBox" value=""
			path="searchString" />
		<input type="submit" id="filterButton"
			value="<spring:message code='search.button' />" />
		<br />
		<br />
		<table class="listTable" id="listTable">
			<thead>
				<tr>
					<th scope="col"><spring:message code="search.name" /></th>
					<th scope="col"><spring:message code="search.code" /></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="unit" items="${searchResult}">
					<tr>
						<td>
							<div id="row_${unit.code}">
								<a href="view/${unit.code}" id="view_${unit.code}">${unit.name}</a>
							</div>
						</td>
						<td>${unit.code}</td>
						<td><a href="delete/${unit.code}" id="delete_${unit.code}">
								<spring:message code="search.delete" />
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form:form>
</body>
</html>
