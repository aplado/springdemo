<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SpringDemo</title>
<style type="text/css">
<!--
@import url("<c:url value='/static/style.css' />");
-->
</style>
</head>
<body>
	<%@ include file="menu.jsp"%>
	<form:form method="post" action="save" modelAttribute="addForm">

		<form:errors path="*" id="messageBlock" cssClass="errorBlock"
			element="div" />

		<table class="formTable" id="formTable">
			<tbody>
				<tr>
					<td><spring:message code="form.name" />:</td>
					<td><form:input id="nameBox" path="unit.name" /></td>
				</tr>
				<tr>
					<td><spring:message code="form.code" />:</td>
					<td><form:input id="codeBox" path="unit.code" /></td>
				</tr>
				<tr>
					<td><spring:message code="form.superunit" />:</td>
					<td><form:select id="superUnitCode" path="superUnitCode">

							<c:forEach items="${unitNames}" var="unit">
								<form:option value="${unit.code}">${unit.name}</form:option>
							</c:forEach>

							<form:option value="${unit.code}">${unit.name}</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td><spring:message code="form.subunits" />:</td>
					<td></td>
				</tr>
				<tr>
					<td><spring:message code="form.channels" />:</td>
					<td></td>
				</tr>

				<tr>
					<td></td>
					<td><c:forEach items="${addForm.channels}" varStatus="status">
							<form:input path="channels[${status.index}].id"
								id="channels${status.index}.id"
								name="channels[${status.index}].id" disabled="${'true'}"
								type="hidden" />

							<form:select path="channels[${status.index}].type"
								items="${addForm.map}">
							</form:select>

							<form:input path="channels[${status.index}].number"
								id="channels${status.index}.value" />
								
							<spring:message code="form.deletechannel" var="deleteChannel" />
							<form:input type="submit"
								path="channels[${status.index}].deleteButton"
								value="${deleteChannel}"
								id="channels${status.index}.pressed" />
							<br />


						</c:forEach></td>
				</tr>

				<tr>
					<td colspan="2" align="right"><input id="addChannelButton"
						name="addChannelButton"
						value="<spring:message code='form.addchannel' />"
						class="linkButton" type="submit" value="" /></td>
				</tr>

				<tr>
					<td colspan="2" align="right"><br /> <input type="submit"
						value="<spring:message code='form.add' />" id="addButton" /></td>
				</tr>
			</tbody>
		</table>
	</form:form>
</body>

</html>



