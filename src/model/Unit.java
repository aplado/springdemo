package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Unit {

	@Id
	@SequenceGenerator(name = "my_seq", sequenceName = "SEQ1", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
	private Long id;
	
    @NotNull
    @Size(min = 2, max = 15)
	private String name;
    
    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "([_a-zA-Z0-9]+)")
	private String code;
    
	private Long super_unit_id;
	
    @OneToMany(
    	      mappedBy = "unit",
    	      fetch = FetchType.EAGER,      
    	      cascade = {CascadeType.PERSIST}, 
    	      orphanRemoval = true
    	      )
	private List<Channel> channels;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getSuper_unit_id() {
		return super_unit_id;
	}

	public void setSuper_unit_id(Long super_unit_id) {
		this.super_unit_id = super_unit_id;
	}

	public List<Channel> getChannels() {
		return channels;
	}

	public void setChannels(List<Channel> channels) {
		this.channels = channels;
	}

	public void addChannels(List<Channel> channels) {
        if (this.channels == null) this.channels = new ArrayList<Channel>();
        
        for (Channel c : channels) {
         c.setUnit(this);
         this.channels.add(c);
        }
     }
}
