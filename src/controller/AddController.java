package controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import model.Channel;
import model.Unit;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import view.UnitForm;
import dao.Dao;

@Controller
public class AddController {

	@Resource
	private Dao dao;

	@RequestMapping("/addForm")
	public String addForm(@ModelAttribute("addForm") UnitForm form,
			ModelMap model) {
		List<Unit> unitNames = new ArrayList<Unit>();
		unitNames = dao.findAllDepartments();
		unitNames.add(0, new Unit());
		model.addAttribute("unitNames", unitNames);
		form.setMap(dao.getChannelTypes());
		return "add";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(ModelMap model,
			@Valid @ModelAttribute("addForm") UnitForm saveForm,
			BindingResult result) {
		String code = saveForm.getSuperUnitCode();
		Unit unit = saveForm.getUnit();



			if (saveForm.getChannelWithDeletePressed() != null) {
				saveForm.removeChannel(saveForm.getChannelWithDeletePressed());
				return "add";
			} else if (saveForm.getAddChannelButton() != null) {
				saveForm.getChannels().add(new Channel());
				return "add";
			} else {
				Unit units = saveForm.getUnit();
				units.addChannels(saveForm.getChannels());
				if (code != null && !code.isEmpty()) {
					Long superUnit = dao.getUnitByCode(code).getId();
					unit.setSuper_unit_id(superUnit);
				}
				units.setChannels(saveForm.getChannels());
			if (result.hasErrors()) {
				return "add";
			}
				dao.save(units);
			return "redirect:/search";
		}
	}
}