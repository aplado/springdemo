package controller;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import view.UnitForm;
import dao.Dao;
import model.Unit;

@Controller
public class ViewController {
	
    @Resource
    private Dao dao;
    
	@RequestMapping("/view/{unitCode}")
	public String view(@ModelAttribute("viewForm") UnitForm form,
			@PathVariable("unitCode") String unitCode, ModelMap model) {
		Unit unit = dao.getUnitByCode(unitCode);
		if (unit != null) {
			form.setUnit(unit);
			Unit superUnit;
			if (unit.getSuper_unit_id() == null) {
				superUnit = new Unit();
				superUnit.setName("");
				superUnit.setCode("");
			} else {
				superUnit = dao.getUnitById(unit.getSuper_unit_id());
			}
			model.addAttribute("superUnit", superUnit);
			model.addAttribute("subUnits", dao.getSubUnitById(unit.getId()));
			form.setChannels(unit.getChannels());
			form.setMap(dao.getChannelTypes());
		}
		return "view";
	}
	
	@RequestMapping("/view")
	public String view() {
		return "view";
	}
}