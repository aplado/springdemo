package controller;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import dao.Dao;

@Controller
public class ManageDataController {
	
    @Resource
    private Dao dao;
	
	@RequestMapping(value = "/delete/{unitCode}")
	public String delete(@PathVariable("unitCode") String code) {
		System.out.println("Data deleted with code:" + code);
		dao.deleteDataById(dao.getUnitByCode(code).getId());
		return "redirect:/search";
	}
	
	@RequestMapping(value = "/admin/clearData")
	public String deleteAllData() {
		dao.deleteAllData();
		return "redirect:/search";
	}
}