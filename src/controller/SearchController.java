package controller;


import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import view.UnitForm;
import dao.Dao;

@Controller
public class SearchController {
	
    @Resource
    private Dao dao;
	
	@RequestMapping(value="/")
	public String search(@ModelAttribute("unitForm") UnitForm unitForm, ModelMap modelMap){
		modelMap.addAttribute("searchResults", dao.findAllDepartments());
		return "search";
	}
	
	@RequestMapping(value="/search")
	public String searchByUserInput(@ModelAttribute("unitForm") UnitForm unitForm,
			ModelMap model){
			String userInput = unitForm.getSearchString();
			if (userInput == null) {
				model.addAttribute("searchResult", dao.findAllDepartments());
			} else {
				model.addAttribute("searchResult",
						dao.fullTextSearch(userInput));
			}
			return "search";
	}
}
